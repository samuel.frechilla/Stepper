import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StepperCustomComponent } from './stepper-custom.component';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [StepperCustomComponent]
})
export class StepperCustomModule { }
