import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-stepper-custom',
  templateUrl: './stepper-custom.component.html',
  styleUrls: ['./stepper-custom.component.css']
})
export class StepperCustomComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  isClickStepOne: boolean;
  isClickStepTwo:boolean;

  constructor(private _formBuilder: FormBuilder) { }

  ngOnInit() {
     this.firstFormGroup = this._formBuilder.group({
      fisrtNameCtrl: ['', Validators.required],
      lastNameCtrl: ['', Validators.required],
      phoneNumberCtrl: ['', Validators.compose([Validators.required, Validators.maxLength(9), Validators.minLength(9), Validators.pattern('(6|7)[0-9]+')])]
    });
    this.secondFormGroup = this._formBuilder.group({
      addressCtrl: ['', Validators.required],
      cityCtrl: ['', Validators.required],
      postalCodeCtrl: ['', Validators.compose([Validators.required, Validators.maxLength(5), Validators.pattern('[0-9]{5}')])]
    });

    this.isClickStepOne = false;
    this.isClickStepTwo = false;


  }

   public stepOne() {
    this.isClickStepOne = true;
  }

   public stepTwo() {
    this.isClickStepTwo = true;
  }

}
