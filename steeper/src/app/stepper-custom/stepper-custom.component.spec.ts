import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepperCustomComponent } from './stepper-custom.component';

describe('StepperCustomComponent', () => {
  let component: StepperCustomComponent;
  let fixture: ComponentFixture<StepperCustomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepperCustomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepperCustomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
