import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MdStepperModule,
          MdButtonModule,
          MdInputModule, } from '@angular/material'
import { ReactiveFormsModule } from '@angular/forms'


import { StepperCustomComponent } from './stepper-custom/stepper-custom.component';
import { NumericOnlyDirective } from './numeric-only.directive';


@NgModule({
  declarations: [
    AppComponent,
    StepperCustomComponent,
    NumericOnlyDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MdInputModule,
    MdButtonModule,
    MdStepperModule,
    ReactiveFormsModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
